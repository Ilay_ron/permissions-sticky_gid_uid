#!/usr/bin/env bash

#created by Ilay Ron
#purpose: permissions, setuid and setgid practice


#this code detect whether someone is root
amroot =(getuid,() == 0);

#root can change any password any time

if (amroot) {
	return;
}

#password detect

ps -e -r |grep passwd

#seting the suid bit

ls -lh htg

,.htg

sudo cp htg /usr/local.bin

sudo chmod u+s /sur/local/bin/htg

ls -hl /usr/local/bin/htg

#the sgid bit

sudo chown root:sports /usr/local/bin/htg

sudo chmod u-s,g+s /usr/local/bin/htg

ls -lh /usr/local/bin/htg

#which group belong

id -G serena

id -G venus

htg

sudo mkdir work

sudo chown serena:tennis work

sudo chmod g+s work

ls -lh -d work

cd /work

mkdir demo

ls -lh -d demo

#The stickty bit

mkdir shared

sudo chmod o+t shared

ls -lh -d /tmp

ls -lh -d /var/tmp

