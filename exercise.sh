!#/usr/bin/env bash

#created by ilay ron
#purpose: sticky, setuid setgid
#################################

#question1

groupadd sports

mkdir /home/sport

chown root:sports /root/home

#question2

chmod 770 /home/sports

#question3

chmod 2770 /home/sports

#question4

chmod +t /home/sports

#question5

chmod 4755 /usr/bin/passwd

ls -l /usr/bin/passwd


